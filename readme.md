## La Horta - **API**

![BuiltBy](https://img.shields.io/badge/Nodejs-LaHorta-orange.svg "img.shields.io")
[![Coverage Status](https://coveralls.io/repos/gitlab/mizamelo22/lahorta/badge.svg?branch=master)](https://coveralls.io/gitlab/mizamelo22/lahorta?branch=master)

`Tests`

Para rodar os testes basta apenas, na raiz do projeto, executar o seguinte comando:

```bash
$ yarn test
```

Ou, dependendo de qual genreciador de dependências vecê utilize:

```bash
$   npm test
```

`Coverage`

Para melhor visualização dos teste realizados, podemos ver no link abaixo que todas as funcionalidades desta aplicação estão devidamente testadas.

[Dashboard Coverage](./__tests__/coverage/lcov-report/index.html)
